// package
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

// Sizes
export const SIZES = {
  hp: hp,
  wp: wp,
};
