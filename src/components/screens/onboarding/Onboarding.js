import {Image, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import React from 'react';

// app intro slider //
import AppIntroSlider from 'react-native-app-intro-slider';

// Constant
import {SIZES} from '../../constants/Constants';

// svg images //
import Biriyani_Svg from '../../../assets/images/onboarding/biriyani.svg';
import Snack_Svg from '../../../assets/images/onboarding/snack.svg';
import Noodles_Svg from '../../../assets/images/onboarding/noodles.svg';

const Onboarding = () => {
  const slides = [
    {
      key: 1,
      text: 'Find the best rated Restaurants near you !',
      image: require('../../../assets/images/onboarding/biriyanipng.png'),
      backgroundColor: '#fff',
    },
    {
      key: 2,
      text: 'Explore Food Spots and Home Bakers. ',
      image: require('../../../assets/images/onboarding/biriyanipng.png'),
      backgroundColor: '#febe29',
    },
    {
      key: 3,
      text: 'Fun with Foodies of your local area.',
      image: require('../../../assets/images/onboarding/biriyanipng.png'),

      backgroundColor: '#22bcb5',
    },
  ];

  // slider functions //
  const renderItem = ({item, index}) => {
    return (
      <View style={{flex: 1, justifyContent: 'space-evenly'}}>
        <View style={{borderWidth: 1}}>
          <Image source={item.image} />
        </View>
        <View style={{borderWidth: 1}}>
          <Text>{item.text}</Text>
        </View>
      </View>
    );
  };
  const renderNextButton = () => {
    return (
      <View style={{}}>
        <View
          style={{
            borderWidth: 1,
            width: 20,
            height: 20,
            backgroundColor: 'red',
          }}></View>

        {/* <Icon
          name="md-arrow-round-forward"
          color="rgba(255, 255, 255, .9)"
          size={24}
        /> */}
      </View>
    );
  };

  const renderDoneButton = () => {
    return (
      <View>
        <View
          style={{
            borderWidth: 1,
            width: 20,
            height: 20,
            backgroundColor: 'red',
          }}></View>
        {/* <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} /> */}
      </View>
    );
  };
  const renderSkipButton = () => {
    return (
      <Text style={{fontSize: 20, color: 'red'}} allowFontScaling={false}>
        Skip
      </Text>
    );
  };

  return (
    <AppIntroSlider
      data={slides}
      renderItem={renderItem}
      renderDoneButton={renderDoneButton}
      renderNextButton={renderNextButton}
      renderSkipButton={renderSkipButton}
      showSkipButton={true}
    />
  );
};

export default Onboarding;

const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
